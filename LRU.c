//
//  LRU.c
//  SystemProgramming_assginment_1
//
//  Created by 화랑 on 2018. 3. 9..
//  Copyright © 2018년 hwarang. All rights reserved.
//

#include <stdio.h>
#include "LRU.h"

 //This function makes new node contatining item_.
 //It will return the new node.
 Node* newNode(int item_) {
     Node *n = (Node*)malloc(sizeof(Node));
     n->item = item_;
     n->next = NULL;
     n->prev = NULL;
     return n;
 }
 
 //This function makes new empty queue with size.
 //It will return empty queue which can contain items up to size.
 Queue* createQueue(int size) {
     
     Queue *newQueue = (Queue*)malloc(sizeof(Queue));
     newQueue->count_ = 0;
     newQueue->queue_size = size;
     
     newQueue->head = NULL;
     newQueue->tail = NULL;
     
     return newQueue;
     
 }
 
 //This function will check whether queue is empty or not.
 //return 1 mean the Queue is Empty, 0 is not Empty.
 int isQueueEmpty(Queue* queue_) {
     
     if(queue_->count_==0){
         return 1;
     }
     return 0;
 }

//This function will check queue have same item.
//return 1 mean the Queue have same item, 0 doesn't have same item.
//if have same item, change the node's position.
int isSameElement(Queue* queue_, int item_){
    
    Node* it = queue_->head;
    
    while (it != NULL) {
        
        if(it->item==item_){
            
            if(it->prev == NULL){

            }else if(it->next == NULL){
                
                DeQueue(queue_);
                EnQueue(queue_,item_);
                
            }else{
                
                int item = it->item;
                it->prev->next = it->next;
                it->next->prev = it->prev;
                free(it);
                EnQueue(queue_,item);
                queue_->count_ -=1;
                
            }
            return 1;
        }
        it = it->next;
    }
    
    return 0;
    
}

//This function will remove last element(tail) in queue_.
//Reference Last Node as A and its prev Node as B. queue's Last Node Save to Memory and
//free A and .
void DeQueue(Queue* queue_) {
    
    Node *n_tail = queue_->tail->prev;
    free(queue_->tail);
    
    queue_->tail = n_tail;
    n_tail->next = NULL;
    queue_->count_-=1;

}

//This function roles add the node containing item_ in queue_ at head.
//Reference First Node.
void EnQueue(Queue* queue_, int item_) {
    
    Node *n = newNode(item_);
    n->next = queue_->head;
    
    if(isQueueEmpty(queue_)){
        queue_->head = n;
        queue_->tail = n;
    }else{
        queue_->head->prev = n;
    }
    queue_->head = n;
    queue_->count_+=1;
    
}

//This function mention item_ in queue. So the queue should be modified along LRU.
//If item_ is not in queue, it should be added,
//If item_ is in queue_, it should be placed in front of queue_.

void Reference(Queue* queue_, int item_) {
    int a;
    if(isSameElement(queue_,item_)){

    }else if(queue_->queue_size <= queue_->count_){
        DeQueue(queue_);
        EnQueue(queue_,item_);
    }else{
        EnQueue(queue_,item_);
    }
}

//This function clear the queue_.
//Start free First Node to Final Node and, count_ = 0. But Not change queue_size.
//And queue's Head And Tail also free.
//Memory Free is Very Important.
void ClearQueue(Queue* queue_) {
    
    Node* it = queue_->head;
    
    while(it != NULL){
        if(it->next == NULL){
            free(it);
            it = NULL;
        }else{
            it = it->next;
            free(it->prev);
        }
    }
    
    queue_->head = NULL;
    queue_->tail = NULL;
    queue_->count_ = 0;
    
}

void PrintQueue(Queue* queue_) {
    Node* it = queue_->head;
    int index = 0;
    printf("------------------------\n");
    printf("Queue size : %d\n", queue_->count_);
    printf("Queue capacity : %d\n", queue_->queue_size);
    while (it != NULL) {
        printf("Queue %d Element : %d\n", index, it->item);
        index++;
        it = it->next;
    }
    printf("------------------------\n");
}

